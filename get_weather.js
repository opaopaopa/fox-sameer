// This is basically the same task you asked me to implement:
// I am sorry I had no time to figure out what was under the hood of 
// getCommits and getBranches methods, so I decided to call weather free api endpoint 
// in the loop and return one object with every response from every request
const Client = require('node-rest-client').Client;
const client = new Client();

let finalResult = [];

getWeather = (data) => {
  let weather = 'unavailable';
  if("weather" in data) {
    // I know that is wrong, sorry, I have no time for make it perfect
    if(data.weather.length > 0) {
      weather = data.weather[0].description;
    }
  } 
  return weather;
}
// get the list of cities from lambda
client.get('https://yqf0c4gn8d.execute-api.us-west-2.amazonaws.com/prod/getCities', (cities, initialResponse) => {
  console.log('cities retrieved');

  cities.forEach(city => {

    client.get(`http://api.openweathermap.org/data/2.5/weather?q=${city}&APPID=a7b03006304006616c32dc9af7bde4e8`, (weatherData, response) => {
      const weather = getWeather(weatherData);
      const result = { weather, city };
      finalResult.push(result);

      // Make sure every request ended
      if(finalResult.length === cities.length) {
        console.log('Congrats!');
        console.log(finalResult);
        return "whatever you want or resolve a promise";
      }

  });

});
});
